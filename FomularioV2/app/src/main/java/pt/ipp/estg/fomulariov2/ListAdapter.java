package pt.ipp.estg.fomulariov2;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private ArrayList<User> list;
    private Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public ListAdapter(ArrayList<User> list){
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cv = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        CardView layout = viewHolder.layout;
        TextView userName = layout.findViewById(R.id.user_name);
        userName.setText(list.get(position).getUserName());
        TextView email = layout.findViewById(R.id.email);
        email.setText(list.get(position).getEmail());
        layout.setOnClickListener(v -> {
            if (listener != null) listener.onClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{

        CardView layout;

        public ViewHolder(@NonNull CardView itemView) {
            super(itemView);
            layout = itemView;
        }
    }

    interface Listener {
        void onClick(int pos);
    }
}
