package pt.ipp.estg.fomulariov2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.EditText;

import java.util.ArrayList;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String TAG = DataBaseHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "user_db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_NAME = "users";
    private static final String FIRST_ROW = "id";
    private static final String SECOND_ROW = "user_name";
    private static final String THIRD_ROW = "name";
    private static final String FORTH_ROW = "email";
    private static final String FIFTH_ROW = "mobile";

    public DataBaseHelper(@NonNull Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + "( " +
                FIRST_ROW + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                SECOND_ROW + " VARCHAR(50) NOT NULL, " +
                THIRD_ROW + " VARCHAR(50) NOT NULL, " +
                FORTH_ROW + " VARCHAR(50) NOT NULL, " +
                FIFTH_ROW + " VARCHAR(50) NOT NULL )");

        db.execSQL("INSERT INTO " + TABLE_NAME + "( "
                + SECOND_ROW + ", " + THIRD_ROW + ", " + FORTH_ROW + ", "  + FIFTH_ROW + ")" +
                " VALUES ('Zyr0', 'João', 'skidrow.jc@gmail.com', '999111999')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void getAll(ArrayList<User> list){
        list.clear();
        try (SQLiteDatabase db = getReadableDatabase()) {

            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME + ";", null);

            if (c != null && c.moveToFirst()) {
                do {
                    User u = new User(
                            c.getInt(0),
                            c.getString(1),
                            c.getString(2),
                            c.getString(3),
                            c.getString(4));
                    list.add(u);
                } while (c.moveToNext());
                c.close();
            }

        } catch (Exception e) {
            Log.e(TAG, "getAll: ", e);
        }
    }

    public User getUser(int id){
        User u = null;
        try (SQLiteDatabase db = getReadableDatabase()){
            String[] params = {String.valueOf(id)};

            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = ?", params);

            if (c != null && c.moveToFirst()) {
                do {
                    u = new User(
                            c.getInt(0),
                            c.getString(1),
                            c.getString(2),
                            c.getString(3),
                            c.getString(4));
                } while (c.moveToNext());
                c.close();
            }

        } catch (Exception e) {
            Log.e(TAG, "getUser: ", e);
        }
        return u;
    }

    public boolean updateUser(int id, String userName, String name, String email, String mobile) {

        try (SQLiteDatabase db = getWritableDatabase()){

            ContentValues cv = new ContentValues();
            cv.put(SECOND_ROW, userName);
            cv.put(THIRD_ROW, name);
            cv.put(FORTH_ROW, email);
            cv.put(FIFTH_ROW, mobile);

            String[] args = { String.valueOf(id) };

            db.update(TABLE_NAME, cv,"id = ?", args);

        } catch (Exception e) {
            Log.e(TAG, "updateUser: ", e);
            return false;
        }
        return true;
    }

    public boolean addUser(String userName, String name, String email, String mobile) {

        try (SQLiteDatabase db = getWritableDatabase()){

            ContentValues cv = new ContentValues();
            cv.put(SECOND_ROW, userName);
            cv.put(THIRD_ROW, name);
            cv.put(FORTH_ROW, email);
            cv.put(FIFTH_ROW, mobile);

            db.insert(TABLE_NAME,null, cv);

        } catch (Exception e) {
            Log.e(TAG, "addUser: ", e);
            return false;
        }
        return true;
    }
}
