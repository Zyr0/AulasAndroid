package pt.ipp.estg.fomulariov2;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

public class FormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        int id = getIntent().getIntExtra("id", -1);
        String type = getIntent().getStringExtra("type");

        ActionBar action = getSupportActionBar();
        if (action != null){
            action.setDisplayHomeAsUpEnabled(true);
            action.setTitle(type);
        }

        FloatingActionButton floatingActionButton = findViewById(R.id.btn_floating);
        EditText userName = findViewById(R.id.get_username);
        EditText name = findViewById(R.id.get_name);
        EditText email = findViewById(R.id.get_email);
        EditText mobile = findViewById(R.id.get_mobile);

        if (id != -1){
            floatingActionButton.setImageResource(R.drawable.ic_edit_white_24dp);
            DataBaseHelper helper = new DataBaseHelper(this);
            User u = helper.getUser(id);
            userName.setText(u.getUserName());
            name.setText(u.getName());
            email.setText(u.getEmail());
            mobile.setText(u.getMobile());
        }
        else {
            floatingActionButton.setImageResource(R.drawable.ic_add_white_24dp);
        }

        DataBaseHelper helper = new DataBaseHelper(this);

        floatingActionButton.setOnClickListener(v -> {
            boolean ok;
            String text;
            if (id != -1){
                ok = helper.updateUser(id, userName.getText().toString(), name.getText().toString(),
                        email.getText().toString(), mobile.getText().toString());
                text = ok ? "User Updated" : "Error";
            }
            else {
                ok = helper.addUser(userName.getText().toString(), name.getText().toString(),
                        email.getText().toString(), mobile.getText().toString());
                text = ok ? "User Created" : "Error";
            }
            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        });
    }
}
