package pt.ipp.estg.fomulariov2;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserListFragment extends Fragment {

    private ArrayList<User> list;

    public UserListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        list = new ArrayList<>();
        DataBaseHelper helper = new DataBaseHelper(context);
        helper.getAll(list);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater
                .inflate(R.layout.fragment_user_list, container, false);

        ListAdapter adapter = new ListAdapter(list);
        recyclerView.setAdapter(adapter);
        recyclerView.setClickable(true);
        recyclerView.setFocusable(true);

        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter.setListener(pos -> {
            Intent intent = new Intent(getContext(), ShowUserActivity.class);
            intent.putExtra("id", list.get(pos).getId());
            startActivity(intent);
        });

        return recyclerView;
    }
}
