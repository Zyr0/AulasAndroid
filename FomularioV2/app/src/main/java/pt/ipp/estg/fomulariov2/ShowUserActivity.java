package pt.ipp.estg.fomulariov2;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class ShowUserActivity extends AppCompatActivity {

    private User u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_user);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }

        int id = getIntent().getIntExtra("id", -1);

        DataBaseHelper helper = new DataBaseHelper(this);

        u = helper.getUser(id);

        TextView userName = findViewById(R.id.user_name);
        userName.setText(u.getUserName());

        TextView name = findViewById(R.id.name);
        name.setText(u.getName());

        TextView email = findViewById(R.id.email);
        email.setText(u.getEmail());

        TextView mobile = findViewById(R.id.mobile);
        mobile.setText(u.getMobile());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_edit:
                Intent intent = new Intent(this, FormActivity.class);
                intent.putExtra("id", u.getId());
                intent.putExtra("type", "Edit " + u.getUserName());
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
