package pt.ipp.estg.openweather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements AsyncInfo.AsyncResult,
        View.OnClickListener {

    private ProgressBar seek;
    private TableLayout table;
    private TextView name, temp, city, maxTemp, minTemp, humidity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_info).setOnClickListener(this);
        table = findViewById(R.id.table);
        table.setVisibility(View.GONE);

        seek = findViewById(R.id.seek);
        seek.setVisibility(View.GONE);

        name = findViewById(R.id.name);
        city = findViewById(R.id.city);
        temp = findViewById(R.id.temp);
        maxTemp = findViewById(R.id.max_temp);
        minTemp = findViewById(R.id.min_temp);
        humidity = findViewById(R.id.humidity);
    }

    @Override
    public void pre() {
        seek.setVisibility(View.VISIBLE);
    }

    @Override
    public void post(JSONObject result) {
        if (result != null) {
            try {
                if (result.has("ERROR")) {
                    Toast.makeText(this, result.getString("ERROR"), Toast.LENGTH_SHORT).show();
                } else {
                    city.setText(result.getString("name"));

                    JSONObject main = result.getJSONObject("main");
                    temp.setText(main.getString("temp"));
                    humidity.setText(main.getString("humidity"));
                    minTemp.setText(main.getString("temp_min"));
                    maxTemp.setText(main.getString("temp_max"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            table.setVisibility(View.VISIBLE);
        }
        seek.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_info:
                AsyncInfo info = new AsyncInfo(this);
                info.execute(
                        "https://api.openweathermap.org/data/2.5/weather?q= " +
                                name.getText().toString() +
                                "&units=metric&APPID=7ea95bdb0b55ea985bf92bef6a00f656");
                break;
        }
    }
}
