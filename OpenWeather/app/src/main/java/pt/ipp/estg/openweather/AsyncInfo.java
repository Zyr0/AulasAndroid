package pt.ipp.estg.openweather;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AsyncInfo extends AsyncTask<String, JSONObject, JSONObject> {

    private AsyncResult listener;

    AsyncInfo(Context context){
        this.listener = (AsyncResult) context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        listener.pre();
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        HttpURLConnection connection = null;
        InputStream inStream = null;
        BufferedReader bufferedReader = null;

        try {
            URL url = new URL(params[0]);

            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("GET");

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK){
                return new JSONObject("ERROR: " + connection.getResponseCode());
            }

            inStream = connection.getInputStream();

            bufferedReader = new BufferedReader(new InputStreamReader(inStream));

            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null){
                builder.append(line).append("\n");
            }

            return new JSONObject(builder.toString());
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (bufferedReader != null){
                    bufferedReader.close();
                }
                if (inStream != null) {
                    inStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        super.onPostExecute(json);
        listener.post(json);
    }

    public interface AsyncResult {
        void pre();
        void post(JSONObject result);
    }
}
