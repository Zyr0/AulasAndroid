package pt.ipp.estg.citylist;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class PlaceListFragment extends ListFragment {

    private Listener listener;
    private ListAdapter adapter;
    private ArrayList<Place> list;
    private Context context;

    public PlaceListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        try {
            this.listener = (Listener) context;
        }
        catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() +
                    " Must implement PlaceListFragment.Listener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.list = setData();
        adapter = new ListAdapter(this.context, list);
        setListAdapter(adapter);
        setEmptyText("Error");
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        String city = this.list.get((int)id).getCity();
        String country = this.list.get((int)id).getCountry();
        String description = this.list.get((int)id).getDescription();
        double lat = this.list.get((int)id).getLat();
        double lng = this.list.get((int)id).getLng();
        this.listener.onItemClick(id, city, lat, lng, country, description);
    }


    private ArrayList<Place> setData() {
        ArrayList<Place> places = new ArrayList<>();
        places.add(new Place(
                "Setubal",38.5244,-8.8882,"Portugal",null));
        places.add(new Place(
                "Santarem",39.23333,-8.68333,"Portugal",null));
        places.add(new Place(
                "Portalegre",39.29379,-7.43122,"Portugal",null));

        places.add(new Place(
                "Lisbon",38.71667,-9.13333,"Portugal","Lisbon " +
                "is the capital city and largest city of Portugal with a population of 547,631 " +
                "within its administrative limits on a land area of 84.8 km2 (33 sq mi). The urban " +
                "area of Lisbon extends beyond the administrative city limits with a population " +
                "of over 3 million on an area of 958 km2 (370 sq mi), making it the 11th most " +
                "populous urban area in the European Union."));
        return places;
    }


    interface Listener {
        void onItemClick(long id, String city, double lat, double lng, String country, String description);
    }
}
