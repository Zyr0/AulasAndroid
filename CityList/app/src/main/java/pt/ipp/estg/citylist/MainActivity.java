package pt.ipp.estg.citylist;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;

public class MainActivity extends AppCompatActivity implements PlaceListFragment.Listener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(findViewById(R.id.list_fragment) == null){

            PlaceListFragment list = new PlaceListFragment();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, list);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }


    @Override
    public void onItemClick(long id, String city, double lat, double lng, String country, String description) {
        View fragmentView = findViewById(R.id.fragment_container);

        if (fragmentView != null) {
            FragmentMap details = new FragmentMap();
            details.palace(new LatLng(lat, lng), city);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, details);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(null);
            ft.commit();
        }
    }
}


/*details.setCityText(city);
            details.setCountryText(country);
            details.setDetailsText(description);*/