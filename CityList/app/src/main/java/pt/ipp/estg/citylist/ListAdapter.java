package pt.ipp.estg.citylist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ListAdapter extends ArrayAdapter<Place> {

    private ArrayList<Place> list;
    private Context context;

    public ListAdapter(@NonNull Context context, ArrayList<Place> list) {
        super(context, R.layout.row_layout);
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_layout, null);
        }

        TextView city = v.findViewById(R.id.city);
        city.setText(list.get(position).getCity());

        TextView country = v.findViewById(R.id.country);
        country.setText(list.get(position).getCountry());

        return v;
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
