package pt.ipp.estg.citylist;

public class Place {
    private String city;
    private String country;
    private String description;

    private double lat;
    private double lng;

    public Place(String city, double lat, double lng, String country, String description) {
        this.city = city;
        this.lat = lat;
        this.lng = lng;
        this.country = country;
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
