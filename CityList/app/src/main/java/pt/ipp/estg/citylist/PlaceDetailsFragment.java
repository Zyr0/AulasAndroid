package pt.ipp.estg.citylist;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceDetailsFragment extends Fragment {

    private String cityText, countryText, detailsText;

    public PlaceDetailsFragment() {

    }

    public void setCityText(String cityText) {
        this.cityText = cityText;
    }

    public void setCountryText(String countryText) {
        this.countryText = countryText;
    }

    public void setDetailsText(String detailsText) {
        this.detailsText = detailsText;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_place_details, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            this.cityText = savedInstanceState.getString("city");
            this.countryText = savedInstanceState.getString("country");
            this.detailsText = savedInstanceState.getString("details");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        View v = getView();
        if (v != null){

            TextView city = v.findViewById(R.id.city);
            city.setText(cityText);

            TextView country = v.findViewById(R.id.country);
            country.setText(countryText);

            if (detailsText != null){
                TextView description = v.findViewById(R.id.description);
                description.setText(detailsText);
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("city", this.cityText);
        outState.putString("country", this.countryText);
        outState.putString("details", this.detailsText);
    }
}
