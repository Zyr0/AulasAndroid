package pt.ipp.estg.countrylist;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class PreferencesActivity extends PreferenceActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    static final String SHORT_PREFERENCE_KEY = "pref_short_type";
    static final String SHORT_PREFERENCE_DEFAULT = "Show All";

    SharedPreferences pref;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        pref = PreferenceManager.getDefaultSharedPreferences(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        pref.registerOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SHORT_PREFERENCE_KEY,
                sharedPreferences.getString(key, SHORT_PREFERENCE_DEFAULT));
        editor.apply();
    }

    @Override
    protected void onStop() {
        super.onStop();
        pref.unregisterOnSharedPreferenceChangeListener(this);
    }
}
