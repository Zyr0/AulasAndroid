package pt.ipp.estg.countrylist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {


    private final ArrayList<Pais> list;
    private Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public ListAdapter(ArrayList<Pais> list) {
        this.list = list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        TableLayout layout = (TableLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_row, viewGroup, false);
        return new ViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        TableLayout layout = viewHolder.layout;
        TextView textName = layout.findViewById(R.id.name);
        TextView textContient = layout.findViewById(R.id.continente);
        textName.setText(this.list.get(position).getName());
        textContient.setText(this.list.get(position).getContinent());
        layout.setOnClickListener(v -> {
            if (listener != null){
                listener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TableLayout layout;

        ViewHolder(@NonNull TableLayout v) {
            super(v);
            layout = v;
        }
    }

    interface Listener {
        void onClick(int pos);
    }
}
