package pt.ipp.estg.countrylist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Pais> list;
    private ListAdapter adapter;
    private String pref_settings;
    private DataBaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new DataBaseHelper(getApplicationContext());

        list = new ArrayList<>();

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        setUpView(recyclerView);
    }

    private void setUpView(RecyclerView recyclerView) {
        adapter = new ListAdapter(list);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);

        RecyclerView.ItemDecoration decoration =
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(decoration);

        adapter.setListener(pos -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Remove " + list.get(pos).getName())
                    .setMessage("Are you shore that you want to remove " + list.get(pos).getName() +
                    " from the list")
                    .setPositiveButton("YES", (dialog, which) -> {
                        db.delete(list, pos);
                        list.remove(pos);
                        adapter.notifyItemRemoved(pos);
                    })
                    .setNegativeButton("NO", (dialog, which) -> dialog.dismiss());
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        recyclerView.setClickable(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        pref_settings = settings.getString(PreferencesActivity.SHORT_PREFERENCE_KEY,
                PreferencesActivity.SHORT_PREFERENCE_DEFAULT);

        if (pref_settings != null && pref_settings.equals(PreferencesActivity.SHORT_PREFERENCE_DEFAULT)){
            pref_settings = null;
        }

        updateData(pref_settings);
        adapter.notifyDataSetChanged();
    }

    private void updateData(String pref_settings) {
        list.clear();
        db.getCountries(list, pref_settings);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.overflow_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity(new Intent(this, PreferencesActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
