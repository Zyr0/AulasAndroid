package pt.ipp.estg.countrylist;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "lista_paises";
    private static final int DATABASE_VERSION= 1;

    private static final String TABLE_NAME = "paises";
    private static final String FIRST_COL = "id";
    private static final String SECOND_COL = "pais";
    private static final String THIRD_COL = "contiente";
    
    public DataBaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + "(" +
                FIRST_COL + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                SECOND_COL + " VARCHAR(20) NOT NULL," +
                THIRD_COL + " VARCHAR(20) NOT NULL);");

        setValues(db);
    }

    private void setValues(SQLiteDatabase db) {
        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Portugal', 'Europa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Spain', 'Europa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Germany', 'Europa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Netherlands', 'Europa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Norway', 'Europa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Italy', 'Europa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Greece', 'Europa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Poland', 'Europa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('USA', 'America')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Brazil', 'America')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Venezuela', 'America')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Chile', 'America')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Mexico', 'America')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Cuba', 'America')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Argentina', 'America')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Niger', 'Africa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Angola', 'Africa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Egypt', 'Africa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('South Africa', 'Africa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Mozambique', 'Africa')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Japan', 'Asia')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Indonesia', 'Asia')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('Russia', 'Asia')");

        db.execSQL("INSERT INTO " + TABLE_NAME + "(" + SECOND_COL + ", " + THIRD_COL + ") " +
                "VALUES " + "('South Korea', 'Asia')");

    }

    private void dropAll(SQLiteDatabase db){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropAll(db);
        onCreate(db);
    }

    public void delete(ArrayList<Pais> list, int pos){
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "DELETE FROM " + TABLE_NAME +
                " WHERE " + FIRST_COL  + " = " + list.get(pos).getId() + ";";

        try {
            db.execSQL(query);
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void getCountries(ArrayList<Pais> list, String s) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c;

        String query;
        if (s == null){
            query = "SELECT * FROM " + TABLE_NAME;
        }else {
            query = "SELECT * FROM " + TABLE_NAME +
                    " WHERE " + THIRD_COL + " = '" + s + "';";
        }

        c = db.rawQuery(query, null);

        if (c != null && c.moveToFirst()){
            do {
                Pais p = new Pais(c.getInt(0), c.getString(1), c.getString(2));
                list.add(p);
            } while (c.moveToNext());

            c.close();
        }
        db.close();
    }
}
