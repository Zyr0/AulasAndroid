package pt.ipp.estg.downloadfile;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.atomic.AtomicReference;

public class DownloadTask extends AsyncTask<String, Integer, String> {

    private static final String TAG = "LOG";
    private static final AtomicReference<Context> context = new AtomicReference<>();

    public DownloadTask(Context context) {
        DownloadTask.context.set(context);
    }

    @Override
    protected void onPreExecute() {
        Intent intent = new Intent("pt.ipp.estg.downloadfile.UPDATE");
        intent.putExtra("HAHA", -1);
        context.get().sendBroadcast(intent);
    }

    @Override
    protected void onPostExecute(String string) {
        Intent intent = new Intent("pt.ipp.estg.downloadfile.UPDATE");
        intent.putExtra("HAHA", -2);
        context.get().sendBroadcast(intent);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        Intent intent = new Intent("pt.ipp.estg.downloadfile.UPDATE");
        intent.putExtra("HAHA", values[0]);
        context.get().sendBroadcast(intent);
    }

    @Override
    protected String doInBackground(String... args) {
        HttpURLConnection connection = null;
        InputStream inStream = null;
        OutputStream outStream = null;

        try {
            URL url = new URL(args[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("GET");

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.w(TAG, "Error " + connection.getResponseCode() + " " +
                        connection.getResponseMessage());
            }

            int fileLength = connection.getContentLength();
            inStream = connection.getInputStream();
            String path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getPath() + "/downloaded_file.zip";
            outStream = new FileOutputStream(path);

            byte[] data = new byte[fileLength];
            long total = 0;
            int count;
            while ((count = inStream.read(data)) != -1) {
                if (isCancelled()) {
                    inStream.close();
                    return null;
                }
                total += count;
                if (fileLength > 0) {
                    publishProgress((int) (total * 100 / fileLength));
                }
                outStream.write(data, 0, count);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (outStream != null) {
                    outStream.close();
                }
                if (inStream != null) {
                    inStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public interface TaskListener {
        void onUpdate(int val);

        void pre();

        void post();
    }
}
