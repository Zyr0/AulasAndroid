package pt.ipp.estg.downloadfile;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

/**
 * Implementation of App Widget functionality.
 */
public class DownloadWidget extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.download_widget);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        int status = intent.getIntExtra("HAHA", -9);

        if (status != -9) {
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.download_widget);

            switch (status) {
                case -1:
                    views.setTextViewText(R.id.appwidget_text, "Started Download 0%");
                    break;
                case -2:
                    views.setTextViewText(R.id.appwidget_text, "Ended Download 100%");
                    break;
                default:
                    CharSequence text = "Downloading " + String.valueOf(status) + "%";
                    views.setTextViewText(R.id.appwidget_text, text);
                    views.setProgressBar(R.id.progress_horizontal, 100, status, false);
                    break;
            }
            ComponentName widget = new ComponentName(context.getApplicationContext(), DownloadWidget.class);
            AppWidgetManager.getInstance(context).updateAppWidget(widget, views);
        } else {
            super.onReceive(context, intent);
        }
    }
}

