package pt.ipp.estg.downloadfile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA = "url_to_download";
    private static ProgressDialog dialog;
    private static MainActivity mainActivityRunningInstance;
    private EditText editText;

    public static MainActivity getInstance() {
        return mainActivityRunningInstance;
    }

    public static ProgressDialog dialog() {
        return dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dialog = new ProgressDialog(this);
        mainActivityRunningInstance = this;

        Button btn = findViewById(R.id.btn_download);
        btn.setOnClickListener(this);
        editText = findViewById(R.id.url);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_download:
                boolean ok = true;
                String url = "http://ipv4.download.thinkbroadband.com/10MB.zip";
                if (editText.getText() != null){
                    String s = editText.getText().toString();
                    boolean validHttp = URLUtil.isHttpUrl(s);
                    boolean validHttps = URLUtil.isHttpsUrl(s);

                    if (validHttp || validHttps){
                        url = s;
                    } else {
                        Toast.makeText(this, "Bad Url", Toast.LENGTH_SHORT).show();
                        ok = false;
                    }
                }
                if (ok) {
                    Intent service = new Intent(this, DownloadService.class);
                    service.putExtra(EXTRA, url);
                    startService(service);
                }
                break;
        }
    }
}
