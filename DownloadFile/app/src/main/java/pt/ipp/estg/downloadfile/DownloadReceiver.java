package pt.ipp.estg.downloadfile;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DownloadReceiver extends BroadcastReceiver {

    public DownloadReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int status = intent.getIntExtra("HAHA", -9);

        if (MainActivity.getInstance() != null) {
            ProgressDialog progressDialog = MainActivity.dialog();
            if (status != -9) {
                switch (status) {
                    case -1:
                        progressDialog.setMessage("Downloading");
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                        progressDialog.setCancelable(true);
                        progressDialog.setMax(100);
                        progressDialog.show();
                        break;
                    case -2:
                        progressDialog.dismiss();
                        break;
                    default:
                        progressDialog.setProgress(status);
                        break;
                }
            }
        }
    }
}
