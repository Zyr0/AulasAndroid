package pt.ipp.estg.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String NAME = "Main Activity";
    private Button btnToast, btnActivity;
    private EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.text);
        btnToast = findViewById(R.id.btn_toast);
        btnActivity = findViewById(R.id.btn_activity);

        btnToast.setOnClickListener(this);
        btnActivity.setOnClickListener(this);

        Log.d(NAME, "onCreate");
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        String text = editText.getText().toString();
        switch (id){
            case R.id.btn_toast:
                Toast.makeText(this, text,Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_activity:
                Intent intent = new Intent(this, SecondaryActivity.class);
                intent.putExtra(SecondaryActivity.EXTRA,text);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(NAME, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(NAME, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(NAME, "onPause");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(NAME, "onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(NAME, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(NAME, "onDestroy");
    }

}
