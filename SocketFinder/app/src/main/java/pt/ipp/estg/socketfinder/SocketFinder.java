package pt.ipp.estg.socketfinder;

import android.os.AsyncTask;
import android.util.Log;

import java.io.DataInputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Arrays;

public class SocketFinder extends AsyncTask<Void, Void, Void> {

    private static final String TAG = "LOG";
    private static final String HOST = "192.168.1.66";
    private static final int TIMEOUT = 2000;
    private static final int PORT = 3000;

    private int line;

    @Override
    protected void onCancelled() {
        super.onCancelled();
        Log.d("LOG", "onCancelled: CANCELLED");
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (isCancelled()) {
            return null;
        } else {
            try {
                Socket socket = new Socket();
                SocketAddress address = new InetSocketAddress(HOST, PORT);
                socket.connect(address, TIMEOUT);

                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

                DataInputStream in = new DataInputStream(socket.getInputStream());

                String fromServer;
                while ((fromServer = in.readLine()) != null) {
                    Log.d("LOG", "doInBackground: " + fromServer);
                    handleInput(fromServer, out);
                }

                out.close();
                in.close();
                socket.close();
            } catch (Exception e) {
                Log.wtf("LOG", e);
            }
            return null;
        }
    }

    private void handleInput(final String fromServer, PrintWriter out) {
        line++;
        Thread thread = new Thread(() -> {
            String[] numbers;
            switch (line) {
                case 4:
                    out.println(0);
                    break;
                case 5:
                    numbers = fromServer.split(":");
                    String number = numbers[numbers.length - 1];
                    int numberSize = number.trim().length();
                    out.println(numberSize);
                    break;
                case 6:
                    numbers = fromServer.split(" ");
                    int num1 = Integer.parseInt(numbers[numbers.length - 1]);
                    int num2 = Integer.parseInt(numbers[numbers.length - 2]);
                    int num3 = Integer.parseInt(numbers[numbers.length - 3]);
                    if (num1 > num2 && num1 > num3){
                        out.println(num1);
                    } else if (num2 > num1 && num2 > num3) {
                        out.println(num2);
                    } else {
                        out.println(num3);
                    }
                    break;
                case 7:
                    String[] phrase = fromServer.split(":");
                    String[] toReverse = phrase[phrase.length - 1].split(" ");
                    StringBuilder builder = new StringBuilder();
                    for (int i = toReverse.length - 1; i > 0; i--) {
                        builder.append(toReverse[i]);
                        if (i > 1){
                            builder.append(" ");
                        }
                    }
                    String reversed = builder.toString();
                    out.println(reversed);
                    break;
                case 8:
                    out.println(8170348);
                    break;
            }
        });
        thread.start();
    }
}
