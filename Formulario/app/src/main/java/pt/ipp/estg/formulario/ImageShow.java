package pt.ipp.estg.formulario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class ImageShow extends AppCompatActivity {

    static final String SRC = "SRC";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_show);

        String id = getIntent().getStringExtra(SRC);
        ImageView image = findViewById(R.id.image);
        image.setImageResource(getResources().getIdentifier(id, "mipmap", getPackageName()));

    }
}
